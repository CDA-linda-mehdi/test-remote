package com.afpa.rattrapage.dto;

import java.util.List;

import com.afpa.rattrapage.entities.Point;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class SegmentDto {
	
	private int id;
	
//	private PointDto pointA;
//	
//	private PointDto pointB;

	private List<PointDto> listPoints;
	
	private double longueur;
	
//	private String nom;
}
