package com.afpa.rattrapage.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class PointDto {
	
	private int id;
	
	private double abs;
	
	private double ord;
	
	private String nom;
	
	private List<SegmentDto> listSegments;

}
