package com.afpa.rattrapage.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.rattrapage.entities.Point;
import com.afpa.rattrapage.entities.Segment;

@Repository
public interface IDaoSegment extends CrudRepository<Segment, Integer> {

	Optional<Segment> findById(int i);
	
	@Query(value = "SELECT * FROM SEGMENTS_POINTS WHERE id_segment = :id", nativeQuery = true)
	List<Point> listPointById(int id);
}
