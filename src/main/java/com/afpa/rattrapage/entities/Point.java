package com.afpa.rattrapage.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class Point {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "t_abs")
	private double abs;
	
	@Column(name = "t_ord")
	private double ord;
	
	@Column(name = "t_nom", unique = true)
	private String nom;
	
	@ManyToMany(mappedBy = "listPoints")
	private List<Segment> listSegments;

//	private static List<Point> liste = new ArrayList<Point>();
//	
//	public Point(String nom, double abs, double ord) throws PointException {
//		this.abs = abs;
//		this.ord = ord;
//		boolean avecMemeNomTrouvee = false;
//		for (Point point : liste) {
//			if (point.getNom().equalsIgnoreCase(nom)) {
//				throw new PointException();
//			}
//		}
//		if (! avecMemeNomTrouvee) {
//			liste.add(this);
//		}
//	}

}
