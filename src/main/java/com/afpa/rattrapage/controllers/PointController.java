package com.afpa.rattrapage.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afpa.rattrapage.dto.PointDto;
import com.afpa.rattrapage.dto.SegmentDto;
import com.afpa.rattrapage.service.IPointService;

@RestController
@RequestMapping("/points")
public class PointController {
	
	@Autowired
	IPointService pointServ;
	
	@GetMapping("/{id}")
	public PointDto pointById(@PathVariable String id) {
		return this.pointServ.pointById(Integer.parseInt(id));
	}
	
	@GetMapping
	public List<PointDto> listPoint() {
		return this.pointServ.listPoint();
	}
	
	@PostMapping
	public PointDto addPoint(@RequestBody PointDto pointdto) {
		PointDto pdto =  this.pointServ.addPoint(pointdto);
		return pdto;
	}
	
	@PutMapping
	public void putPoint(@RequestBody PointDto pointdto) {
		this.pointServ.putPoint(pointdto);
	}
	
	@DeleteMapping("/{id}")
	public PointDto deletePoint(@PathVariable String id) {
		return this.pointServ.deletePoint(Integer.parseInt(id));
	}
	
	@PostMapping("/list/{idS}/{idP}")
	public void addSegmentInPoint(@PathVariable String idS, @PathVariable String idP) {
		this.pointServ.addSegmentInPoint(Integer.parseInt(idS), Integer.parseInt(idP));
	}
	
	@DeleteMapping("/list/{idS}/{IdP}")
	public void deleteSegmentInPoint(@PathVariable String IdS, @PathVariable String idP) {
		this.pointServ.deleteSegmentInPoint(Integer.parseInt(IdS), Integer.parseInt(idP));
	}
	
	@GetMapping("/list/{idP}")
	public List<SegmentDto> listSegmentInPoint(@PathVariable String id) {
		PointDto pDto = new PointDto();
		pDto.setId(Integer.parseInt(id));
		return this.pointServ.listSegmentInPoint(pDto);
	}
	

}
