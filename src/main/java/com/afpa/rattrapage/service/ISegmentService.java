package com.afpa.rattrapage.service;

import java.util.List;

import com.afpa.rattrapage.dto.PointDto;
import com.afpa.rattrapage.dto.SegmentDto;
import com.afpa.rattrapage.entities.Point;

public interface ISegmentService {

	SegmentDto segmentById(SegmentDto segmentDto);

	List<SegmentDto> listSegment();

	String addSegment(int idPa, int Pb);

	void putSegment(SegmentDto segmentDto);

	void deleteSegment(int parseInt);
	
	void addPointInSegment(int idPoint, int idSegment);
	
	void deletePointInSegment(int idPoint, int idSegment);
	
	List<PointDto> listPointInSegment(int parseInt);
	
	double longueur(int idS, Point a, Point b);
}
